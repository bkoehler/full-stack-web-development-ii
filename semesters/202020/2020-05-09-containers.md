---
layout: page
title:  "Containers"
date:   2020-05-09 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- distinguish between a container and a virtual machine
- create a container
- mount a volume 
- map a port
- list and remove containers
- create an image
- remove an image


## Resources

- [Containerization Explained](https://youtu.be/0qotVMX-J5s){:target="_blank"} (video)
- [What is a Container?](https://youtu.be/EnJ7qX9fkcU){:target="_blank"} (video)
- [A Beginner-Friendly Introduction to Containers, VMs, and Docker](https://www.freecodecamp.org/news/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b/){:target="_blank"}
- [slides](slides/containers.html){:target="_blank"}
- [narated slides](https://youtu.be/fiTeCOnis7Q){:target="_blank"} (video)

## Lab

[Video walkthrough of this lab](https://youtu.be/_78KcRwzVZE){:target="_blank"}.

### Creating a Container

Start the Google Cloud Shell.

In the terminal, run the following command:

{% highlight shell %}
docker run --rm -it alpine
{% endhighlight %}

`docker` is the Docker client program.

`run` is the subcommand used create a new container.

`--rm` is the option to remove the container when it exits.

`-i` or `--interactive` keeps STDIN open even if not attached.

`-t` or `--tty` allocates a pseudo-TTY. `-i` and `-t` are used together to create a
container that we can interact with directly from the terminal.

`alpine` is the name of image upon which the new container is based.

If everything works, you should see a `# ` shell prompt and should be able
to use most basic shell commands such as `cd`, `ls`, `cat`, `cp`, `touch`, 
`echo`, `rm`, `mv` and `rmdir`.

Try the following:

{% highlight shell %}
cd /root
echo "Hello!" > first-file.txt
ls -la
cat first-file.txt
{% endhighlight %}

### Getting Information About Running Containers

Create a new terminal on the host.

Start another container:

{% highlight shell %}
docker run --rm -it alpine
{% endhighlight %}

Is `first-file.txt` present in the `/root` directory in this container?

Create yet another terminal on the host. Run the following command (ON THE HOST -- not on the containers)
to list the running containers:

{% highlight shell %}
docker ps -s
{% endhighlight %}

Run the following command to get streaming information on the running containers:

{% highlight shell %}
docker stats
{% endhighlight %}

You will have to hit `Ctrl-C` to stop `docker stats`.

Stop one of the contianers by entering `Ctrl-D` or `exit` on its terminal.

Confirm that the container has stopped by using `docker ps` or `docker stats`.

Stop the other container.

Where is `first-file.txt` now?

### Working with Images

You can get a list of locally available images using the following command:

{% highlight shell %}
docker images
{% endhighlight %}

If you are running out of disk space on your host, you can remove images that
you are no longer using as follows:

{% highlight shell %}
docker rmi cc0abc535e36
{% endhighlight %}

Here, `cc0abc535e36` is the image ID for the particular image we want to remove.

### Mounting Volumes

Frequently we will have a directory of preexisting files that we would like to
access inside the container or we end up creating files inside the container that
we want to persist beyond the lifetime of the container. In this case, we can use
the `-v` option to `docker run` to make a directory on the host available inside
the container. The value of the `-v` option has the following form: `/path/on/host:/path/inside/container`.
For example, run following commands on the host:

{% highlight shell %}
mkdir site1
cd site1
echo '<h1>First Page!</h1>' > index.html
docker run --rm -it -v $PWD:/mnt/site1 -u 1000:1000 alpine
{% endhighlight %}

Inside the container do the following:

{% highlight shell %}
cd /mnt/site1
cat index.html
echo '<h1>Second Page</h1>' > page2.html
{% endhighlight %}

and then exit the container. What is contained in the current directory on the host now?

### Mapping Ports

As with volumes, we often want to make network services running inside the container
accessible outside the container. In this case, we use the `-p` option to `docker run` to 
map a port on the host to a port inside the container. For example:

{% highlight shell %}
docker run --rm -d -v $PWD:/usr/share/nginx/html:ro -p 8080:80 nginx:alpine
{% endhighlight %}

Notice the use of the `-d` flag instead of `-it`. `-d` stands for daemon. This
container runs in the background and we do not interact with it directly with
the terminal. 

The `-p` flag is used to forward ports on the host to a port inside the container.
In this case we are forwarding port 8080 on the host to port 80 insided the
container. The Google Cloud Shell allows as to view the app running on port 8080 by
clicking on the "Web Preview" icon at the top right of the Cloud Shell window.
if you do this, you should see the `index.html` page you created above. If you
edit the address to `https://{your domain}/page2.html`, you should see `page2.html`.

Because this container is running in the background, in order to stop it you would need to use the 
`docker stop xxx` command where `xxx` is either the name or ID of the container
which you could get from the `docker ps` command.

### Creating Images

Up until this point, we've used existing images from the Docker hub registry. There
are situations in which we would like to define our own images. For example, what 
if we wanted to make the simple two page website above a standalone container
instead of having to mount the directory with the website files inside the
container?

Inside the `site1` directory, create a `Dockerfile` with the following contents:

{% highlight docker %}
FROM nginx:alpine

COPY . /usr/share/nginx/html
{% endhighlight %}

The `FROM` clause identifies the base image, in this case the base
Alpine image with Nginx installed.

The `COPY` clause copies everything from the current directory ('.')
(`index.html` and `page2.html`) into the `/usr/share/nginx/html`
directory.

To build the image, run the following command from inside the `site1`
directory:

{% highlight shell %}
docker build -t mywebsite .
{% endhighlight %}

The `-t` option identifies the tag for the image. The '.' argument identifies
the directory containing the `Dockerfile` to build from. In this case,
the current directory. We now have self-contained container image
that has all the files for the website.

Start up the container as follows:

{% highlight shell %}
docker run --rm -d -p 8080:80 mywebsite
{% endhighlight %}

Clicking on the "Web Preview" button should show the website. Visit
the `index.html` page and `page2.html` to confirm that everything is 
working.

Stop the container using the `docker stop xxx` command.

### Pushing Images to a Container registry

In the Google Cloud Shell environment, we will lose any images we have
built when we exit the shell. If we want a persistent copy of an
image, we can push it to a container registry.

The first thing we have to do is tag the image identifying the registry:

{% highlight shell %}
docker tag mywebsite gcr.io/PROJECT-ID/mywebsite
{% endhighlight %}

You will have to replace `PROJECT-ID` with your actual project ID.

Use the `docker images` command to verify that the image that
the image got tagged correctly.

You can then push your image to the container registry as follows:

{% highlight shell %}
docker push gcr.io/PROJECT-ID/mywebsite
{% endhighlight %}

### Pulling Images from a Container Registry

You can pull images from a container registry to use local.

To convince yourself that this will work, first delete the local
copies of your images as follows:

{% highlight shell %}
docker rmi mywebsite gcr.io/PROJECT-ID/mywebsite
{% endhighlight %}

Then pull a copy from the container registry as follows:

{% highlight shell %}
docker pull gcr.io/PROJECT-ID/mywebsite
{% endhighlight %}

You can use the `docker images` command to verify that a local
copy is available.

You can test it out as follows:

{% highlight shell %}
docker run --rm -d -p 8080:80 gcr.io/PROJECT-ID/mywebsite
{% endhighlight %}

and then preview the web pages as before.

Stop the container when you are finishd using the `docker stop xxx`
command.

### Deploying a Container to Cloud Run

Running the container in the Cloud Shell environment is fine for development
but only you can access it and only while the shell is running. To make
you web application available to the public, it has to be deployed somewhere.
Google Cloud Run is an easy way to deploy containers to the public.

Let's start by creating a simple Express application. First change
to your home directory `cd ~`.

Now, use the Express generator to generate a simple Express app:

{% highlight shell %}
npx express-generator -v ejs myapp
{% endhighlight %}

Change to the app directory and install the dependencies:

{% highlight shell %}
cd myapp
npm install
{% endhighlight %}

Update the `package.json` file so that it also has a `dev` script:

{% highlight javascript %}
  "dev": "nodemon ./bin/www"
{% endhighlight %}

The complete `package.json` file will now look something like this:

{% highlight javascript %}
{
  "name": "myapp",
  "version": "0.0.0",
  "private": true,
  "scripts": {
    "start": "node ./bin/www",
    "dev": "nodemon ./bin/www"
  },
  "dependencies": {
    "cookie-parser": "~1.4.4",
    "debug": "~2.6.9",
    "ejs": "~2.6.1",
    "express": "~4.16.1",
    "http-errors": "~1.6.3",
    "morgan": "~1.9.1"
  }
}
{% endhighlight %}

Now you can run the app:

{% highlight shell %}
PORT=8080 DEBUG=myapp:* npm run dev
{% endhighlight %}

Use the "Web Preview" button to preview the app in the browser.

Make some changes to the `views/index.ejs` file. Refresh
the browser window to confirm the changes are taking effect.

Once you are satisfied that the app is working, stop it by
typing CTRL-C in the terminal.

In the `myapp` directory, create a `Dockerfile` so we can
package up the application as a container image:

{% highlight docker %}
# Use the official lightweight Node.js 12 image.
# https://hub.docker.com/_/node
FROM node:12-alpine

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package*.json ./

# Install production dependencies.
RUN npm install --only=production

# Copy local code to the container image.
COPY . ./

# Run the web service on container startup.
CMD [ "npm", "start" ]
{% endhighlight %}

Create a `.dockerignore` file to exclude files from your container
image:

{% highlight text %}
Dockerfile
README.md
node_modules
npm-debug.log
{% endhighlight %}

Build and tag the image:

{% highlight shell %}
docker build -t gcr.io/PROJECT-ID/myapp .
{% endhighlight %}

You can run the image in a container and test it using
"Web Preview":

{% highlight shell %}
docker run --rm --env PORT=8080 -it -p 8080:8080 gcr.io/PROJECT-ID/myapp
{% endhighlight %}

When you are satisfied the app is working, you can stop it
using CTRL-C.

Push the image to the container registry:

{% highlight shell %}
docker push gcr.io/PROJECT-ID/myapp
{% endhighlight %}

You can deploy the container image to Google Cloud Run as follows:

1. From the Google Cloud Platform Console, choose "Cloud Run" from the menu.
1. Click on the link to "Create Service".
1. For "Deployment platform", choose "Cloud Run (fully managed)" and for
"Region", choose "us-west1 (Oregon)".
1. For "Service name", choose "myapp".
1. For "Authentication", choose "Allow unauthenticated invocations".
1. Click on the "NEXT" button.
1. For "Container image URL", enter "gcr.io/PROJECT-ID/myapp".
1. Click on the "CREATE" button.
1. In a few minutes, you should get a URL for your app that you
can try out.


## Assignment

1. Create a new Express application `calculator`.
1. Add a route for `/add`.
1. On the `/add` page:
    1. Display a form with two text fields "A" and "B".
    1. If the parameters for "A" and "B" are present in
    the request, pre-fill the fields with those values.
    1. If the parameters for "A" and "B" are present in
    the request, display the sum of "A" and "B".
    1. Display a button "Add" that the user can press which
    submits the form with "A" and "B".
1. Add a route for `/multiply`.
1. Make the `/multiply` plage work basically like the
`/add` page except that it calculates the product of
"A" and "B".
1. On the index page ("/") display links to the
`/add` and `/multiply` pages.
1. Test the application locally.
1. Create a "Dockerfile" for the application.
1. Build the image.
1. Test the image locallay.
1. Push the image to the container registry.
1. Deploy the image to Cloud Run.
1. Test.
1. Create a project in GitLab called `calculator` and push the the assignment.
Be sure to create a `.gitignore` file to exclude the `node_modules\` directory.
