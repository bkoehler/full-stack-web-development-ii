---
layout: page
title:  "WebSocket API"
date:   2020-07-04 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- describe the limitations of HTTP for real-time applications
- use polling to address those shortcomings
- implement a real-time application using the WebSocket API

## Resources

- [An Introduction to WebSockets](https://blog.teamtreehouse.com/an-introduction-to-websockets){:target="_blank"}
- [slides](slides/websocket.html){:target="_blank"}

## Lab

[Video of this lab](https://youtu.be/WEPigv8W3dY)

### Test WebSockets in Google Cloud Shell

Clone this project:
    `https://gitlab.com/langarabrian/simple-chat2.git`

Change to the project directory (`cd simple-chat2`) and install
    dependencies (`npm install`).

Run on port 8080 (`PORT=8080 npm start`), preview, and confirm
    that the application works.

Build and deploy to Google Cloud Run:

```shell
gcloud builds submit --tag gcr.io/{your-gcp-project-id-here}/simple-chat2
gcloud run deploy simple-chat2 --image gcr.io/{your-gcp-project-id-here}/simple-chat2 --platform managed --region us-west1 --allow-unauthenticated
```

Confirm the application does not work in Google Cloud Run.

### Test WebSockets in Google Compute Engine

From the GCP Console, go to "Compute Engine \| VM instances".

Click on the "Create" button.

For "Name", enter whatever you want, but "websocket-test" would be
a good choice.

For "Region", "us-west1 (Oregon)" would be a good choice. Make
note of the "Zone". You can go with whatever zone is selected
by the system.

For "Machine type", choose "f1-micro".

In the "Firewall" section, check both options to allow HTTP and HTTPS
traffic.

Click on the "Create" button.

Wait until the instance is in the running state.

Copy the IP address to the clipboard. Later we will
set up TLS for this application. A TLS certificate
must be associated with a domain name, it can't be tied to an IP address.

Back in the GCP Console, go to "Network services \| Cloud DNS".

Select your zone.

Click on the link to "+ ADD RECORD SET".

Create an "A" record for the "DNS name" "chat.4949NN.xyz"
using the IP addres of your VM.

Test that the new domain works. You may have to wait a 
few minutes for it to work.

Back in your Cloud Shell terminal, connect to the new VM as follows:

```shell
gcloud compute ssh --zone us-west1-b websocket-test
```

Install Git, NGINX, and Certbot on the new VM:

```shell
sudo apt-get install git nginx certbot python-certbot-nginx
```

Install Node:

```shell
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Clone the simple chat application:

```shell
git clone https://gitlab.com/langarabrian/simple-chat2.git
```

Change to the project directory (`cd simple-chat2`) and
install the project dependencies (`npm install`)

The default NGINX configuration serves simple files.
We need to change it to proxy requests to the Node
server. The project includes a NGINX server defintion
that is set up to this. Copy it to NGINX configuartion
directory:

```shell
sudo cp etc/nginx/sites-available/default /etc/nginx/sites-available
```

Start the Node application (`PORT=8080 npm start`).

Create a new shell tab in the Google Cloud Shell and connect again
to the new VM:

```shell
gcloud compute ssh --zone us-west1-b websocket-test
```

Restart NGINX:

```shell
sudo systemctl restart nginx
```

Test the application using either the IP address or domain name.

Once the domain name is working, set up TLS for the application:

```shell
sudo certbot --nginx
```

When asked for an email address, use your "@mylangara.ca"
email address.

Agree to the Terms of Service.

Share your email address (or not) as you see fit.

When asked for the domain name, enter "tls-test.4949NN.xyz"
but substitude "NN" with your own digits.

It will take a small amount of time to provision the
TLS certificate. After that you will be asked whether
or not you want to configure NGINX to redirect HTTP
traffic to HTTPS. I would normally enable this, so
choose option "2".

Refresh the application in your browser. You should be redirected
to the HTTPS page.


## Assignment

1. Fork this project:
    `https://gitlab.com/langarabrian/fancy-chat2`
1. Remove the fork relationship and make the project private.
1. Clone the project in the Google Cloud Shell environment.
1. Test the application and make sure you understand how it works.
1. In `public/index.html` add a couple of "button" elements
    just after line 11. Label one "Busy" and the other
    "Available".
1. Add "onclick" event handlers to the buttons that will
    emit a new type of event called "status" that the
    server will deliver to all the other participants
    in the chat. For example, if I click on the "Busy"
    button, everyone else will see a log message
    "Brian is busy". If i click on the "Available" button,
    everyone will see a log message "Brian is available".
    Keep it simple, there is no need to track everyone's 
    status.
1. When you have it working, stage, commit, and push the
    changes to the repository.
1. Get the project working on the VM.
