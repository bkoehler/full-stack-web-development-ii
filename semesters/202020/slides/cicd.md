## CI / CD

### CPSC 2650

XXX---XXX

## Learning Outcomes

- describe the key elements of CI / CD
- explain how CI / CD supports agile SDLC methodologies

XXX---XXX

## Continuous Integration (CI)

- process of frequently integrating code changes
- usually incorporates automated building and testing

XXX---XXX

## Continuous Delivery (CD)

- the process of producing a complete deliverable after each successful integration
- automated

XXX---XXX

## Continous Deployment (CD)

- continuous delivery + deploy to production without intervention
- automated

XXX---XXX

## Agile + CI / CD

- because agile turnaround is so short and frequent, need to automate build and testing
- automated

XXX---XXX

## Project Environments

- development
- build
- test / stage
- production

XXX---XXX

## Development Environments

- developers' own workstations
- includes software development tools
- runs a scaled down version of the production data

XXX---XXX

## Build Environments

- centralized server that performs CI / CD builds
- includes software development tools
- tests against scaled down data

XXX---XXX

## Test / Stage Environments

- centralized server that matches the production environment as closely as possible
- no live data, but similar to what would be encountered in production

XXX---XXX

## Production environment

- server(s) used by public / live customers

XXX---XXX

## Role of Containers

- without containers, risk that differences betweeen
    development, build, test, and production produce
    inconsistencies
- if containers are used in all environments, there
    should be verly little difference betweeen
    development and production
- goal is that creating production system should be
    *automated* and *reproducible*

