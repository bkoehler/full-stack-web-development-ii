## Containers

### CPSC 2650

XXX---XXX

## Learning Outcomes

- describe various application execution models
- describe the essential features of a container system
- highlight the differences between VMs and containers

XXX---XXX

## Bare Metal App A

<img src="https://docs.google.com/drawings/d/e/2PACX-1vRcCKy2mhYdJa8GZlWMJThImUHgva48gDYMfkU6Q-onSvn0kJA6l3KpmcSbIGecYP4RxEbQp1oeGYOC/pub?w=960&amp;h=720">

XXX---XXX

## Bare Metal App B

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSaRTHZLrbZe-PpSI-iH0ldYZt4Fgm22o96xNFdussHaOf_9PiAvNbtozR_jgsw45KRtmmnwIU-V4Kd/pub?w=881&amp;h=518">

XXX---XXX

## Bare Metal App C

<img src="https://docs.google.com/drawings/d/e/2PACX-1vTjZ7x_rGNtKgHNjEpY0breGbPIh4RNX4cFfyHTfn1ZyZUVERHIHXUGyGnKxcQBK9tM8DF_u5sxTRiE/pub?w=881&amp;h=518">

XXX---XXX

Problems with the bare metal app model?

XXX---XXX

## OS Single App A 

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSapCcj4ow32sLVXZTJVkkGVMNDNA29I76YqLeeAzizA0KhPsg640F1zJc4tcssJxr9IUxCjjQDKSqO/pub?w=881&amp;h=518">

XXX---XXX

## OS Single App B 

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSMXyhau-xWSsJV31i17tCS7mwN1JVsFAhF0DcRg-K2grOc1uVjb33ekkBtvBwZfC-_Pe2PO11CtbNx/pub?w=881&amp;h=518">

XXX---XXX

## OS Single App C 

<img src="https://docs.google.com/drawings/d/e/2PACX-1vQqcDzUodSbYR2Q3o58OtiRS_w6HWBQ0fSUV0Db7QUABFdCSQ_M6b3WXU7lzSkmby3h5kzXnhJDeu8w/pub?w=881&amp;h=518">

XXX---XXX

Problems with the OS single app model?

XXX---XXX

## Multiprocessing OS

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSwGTdUsRnPXB6JCZ3DONhj-xR20lhYu8AIlZQQssXmj9ZpQB-6xokAWL6QyIjFfoNY6cOvN-B-7L1A/pub?w=881&amp;h=518">

XXX---XXX

Problems with the multiprocessing OS app model?

XXX---XXX

## Type 2 Hypervisor

<img src="https://docs.google.com/drawings/d/e/2PACX-1vRcHWQ2szslpvRqcAimqgW241aEYLfhhrXQ6LUyGSwp_BZ4UofOa8gISkb8vXoYREWxPDPf0l61kfNt/pub?w=881&amp;h=518">

XXX---XXX

## Type 1 Hypervisor

<img src="https://docs.google.com/drawings/d/e/2PACX-1vQj9t5xBq2S_3tq2vFng0IdsHDf3UH-pnnXC4el4XugovzLDrNpNtmnqgsqArp9-ZV2KQ--pJuB7XCL/pub?w=881&amp;h=518">

XXX---XXX

## Containers

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSqdICdf615vAjwQq3t2dCuDaabQw36i3DqIGUQdWKjmd25n3VeONTTcBeIoJXfulMGrMY2KeeZIIRm/pub?w=881&amp;h=518">

XXX---XXX

## Essential Container Concepts

- Image
- Container
- Daemon 
- Client 

XXX---XXX

## Image

- snapshot of filesystem
- contains binaries and libraries sufficient to create a running container 
- images are hierarchial/layered -- a new image is typically built on top of another image

XXX---XXX

## Container

- running image
- image (RO) + memory + filesystem diff (RW)
- underlying image can be shared accross containers

XXX---XXX

## Daemon

- background process responsible for managing containers running
    on a local host 

XXX---XXX

## Client

- program used by end user to build/push/pull/delete images
- also used to start/stop/delete containers
- interacts with a daemon which may be local or remote

XXX---XXX

## VMs vs Containers

| VMs | Containers |
|-----|------------|
|heavy|light|
|OS is duplicated accross VMs|images can be shared accross containers|
|slow to startup|quick to startup|
|independent OSes|shared underlying OS|
|fixed memory size|memory can grow and shrink (limited by underlying host)|
