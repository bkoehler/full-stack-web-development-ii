## Serverless

### CPSC 2650

XXX---XXX

## Learning Outcomes

- outline the problems associated with scaling the 
    backend of a web application
- distinguish the various XaaS

XXX---XXX

## What are the problems with our current architecture

XXX---XXX

## XaaS (X as a Service)

- IaaS (Infrastructure)
- PaaS (Platform)
- FaaS (Function)
- SaaS (Software)

XXX---XXX

## IaaS (Infrastructure)

- vendor: hardware, VM
- customer: OS, libraries, tools, application code
- e.g. AWS EC2, Google Compute Engine, Azure ???

XXX---XXX

## PaaS (Platform)

- vendor: hardware, VM, OS, libraries, tools
- customer: application code
- e.g. AWS Elastic Beanstalk, Google App Engine, 
    Heroku, Azure ???

XXX---XXX

## FaaS (Function)

- vendor: hardware, VM, OS, libraries, tools
- customer: application code (individual functions)
- "scale to zero"
- e.g. AWS Lambda, Google Cloud Functions, Azure ???

XXX---XXX

## SaaS (Software)

- vendor: hardware, VM, OS, libraries, tools, application code
- customer: $$$, no coding
- e.g. Gmail, Salesforce, MongoDB Atlas?
