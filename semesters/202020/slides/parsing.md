## Parsing

### CPSC 2650

XXX---XXX

## Learning Outcomes

- define the following terms: alphabet, token, grammer, parser
- determine whether a string is in a language defined by a grammar
- use a parser to extract information from a document

XXX---XXX

## Alphabet

- the valid characters in a language
- consider a language for simple arithmetic
    expressions that includes integers
    and variables:

```text
a-z, 0-9, +, -, *, /, (, )
```

XXX---XXX

## Token

- the valid "words" in a language
- usually defined using regular expressions
    based on the alphabet:

```text
<num> :== [0-9]+
<var> :== [a-z]+
<op> :== + | - | * | /
<lp> := (
<rp> := )
```

XXX---XXX

## Grammar

- the valid "sentences" in a language
- usually defined using a context free grammar (CFG):

```text
<expr> :== <num> |                  (1)
           <var> |                  (2)
           <expr> <op> <expr> |     (3)
           <lp> <expr> <rp>         (4)
```

XXX---XXX

## Parser

- the program that converts a stream
    of characters into a stream
    of tokens
- checks to see if stream of tokens
    is valid for the grammar

XXX---XXX

## Valid Example

```text
  (    base   +    27    )    *    rate
^ <lp> <var> <op> <num> <rp> <op> <var>
<lp> ^ <var> <op> <num> <rp> <op> <var> (shift)
<lp> <var> ^ <op> <num> <rp> <op> <var> (shift)
<lp> <expr> ^ <op> <num> <rp> <op> <var> (reduce 2)
<lp> <expr> <op> ^ <num> <rp> <op> <var> (shift)
<lp> <expr> <op> <num> ^ <rp> <op> <var> (shift)
<lp> <expr> <op> <expr> ^ <rp> <op> <var> (reduce 1)
<lp> <expr> ^ <rp> <op> <var> (reduce 3)
<lp> <expr> <rp> ^ <op> <var> (shift)
<expr> ^ <op> <var> (reduce 4)
<expr> <op> ^ <var> (shift)
<expr> <op> <var> ^ (shift)
<expr> <op> <expr> ^ (reduce 2)
<expr> ^ (reduce 3)
```

XXX---XXX

## Invalid Example

```text
  (    base   +    27
^ <lp> <var> <op> <num> 
<lp> ^ <var> <op> <num> (shift)
<lp> <var> ^ <op> <num> (shift)
<lp> <expr> ^ <op> <num> (reduce 2)
<lp> <expr> <op> ^ <num> (shift)
<lp> <expr> <op> <num> ^ (shift)
<lp> <expr> <op> <expr> ^ (reduce 1)
<lp> <expr> ^ (reduce 3)
Uh oh!
```

XXX---XXX

## htmlparser2

- Node package for parsing HTML 
- define event handlers for specific applications

XXX---XXX

## onopentag

```javascript
onopentag( name, attributes )
```

- triggered when processing open tag for `name`
- `attributes` is an object containing the
    attributes withing the open tag

XXX---XXX

## onopentagname

```javascript
onopentagname( name )
```

- like `onopentag` but without the attributes

XXX---XXX

## onattribute

```javascript
onattribute( name, value )
```

- triggered for each attribute within an open
    tag separately

XXX---XXX

## ontext

```javascript
ontext( text )
```

- triggered for the text within an element


XXX---XXX

## onclosetag

```javascript
onclosetag( name )
```

- triggered for the closing tag `name`
