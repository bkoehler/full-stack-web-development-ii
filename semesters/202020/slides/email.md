## Email

### CPSC 2650

XXX---XXX

## Learning Outcomes

- describe the historical context of email 
- describe the common email protocols 
- explain the spam problem 
- implement spam prevention via SPF and DKIM
- deliver transactional or notfication email from a web applicaition

XXX---XXX

## Historical Context

- invented in 1971
- first internet application for the masses
- still going strong today

XXX---XXX

## Email Message Format

```text
headers
<blank line>
body
```

XXX---XXX

## Email Message Format

```text
From: jdoe@example.com
To: msmith@otherco.ca
Subject: TPS report

Hi Mary,

Have you completed your TPS report yet?

John
```

XXX---XXX

## Email agents

- Mail Transfer Agent (MTA)
- Mail Delivery Agent (MDA)
- Mail User Agent (MUA)

XXX---XXX

## Mail Transfer Agent (MTA)

- transfers mail between other MTAs
- delivers mail to MDAs

XXX---XXX

## Mail Delivery Agent (MDA)

- repository for mail for end users
- interacts with MUA via POP or IMAP

XXX---XXX

## Mail User Agent (MUA)

- application program used by end users
    to access email
- e.g. Thunderbird
- recieves email from MDA by POP or IMAP
- outgoing mail to MTA by SMTP

XXX---XXX

## Email protocols

- SMTP
- POP
- IMAP

XXX---XXX

## SMTP

- Simple Mail Transfer Protocol
- outgoing mail from MUAs
- mail between MTAs

XXX---XXX

## POP

- Post Office Protocol
- between MUA and MDA
- typically retrieve and delete from MDA

XXX---XXX

## IMAP

- Internet Message Access Protocol
- between MUA and MDA
- email typically kept on MDA

XXX---XXX

## SPAM problem

- should a MTA deliver every
    correctly formatted email message?

XXX---XXX

## Deterring SPAM

- Sender Policy Framework (SPF)
- DomainKeys Identified Mail (DKIM)
- Block Lists

XXX---XXX

## Sender Policy Framework (SPF)

- domain publishes DNS TXT record indicating where valid email comes from
- MTAs check incoming mail against the From: domain's SPF record
- MTAs can discard or flag as SPAM

XXX---XXX

## DomainKeys Identified Maik (DKIM)

- domain publishes DNS TXT records with public key
- outbound SMTP server sign email headers and body with private key
- recieving MTAs can validate signatures
- MTAs can discard or flag as SPAM

XXX---XXX

## Block Lists

- list of IP address for which MTAs should not relay email
- MTAs check list for every incoming email
- if the connecting IP address is on the block list, 
    the MTA can discard or flag as SPAM

XXX---XXX

## Summary

- almost impossible today to run your own outbound MTA
    with high deliverability
- use a service provider instead
