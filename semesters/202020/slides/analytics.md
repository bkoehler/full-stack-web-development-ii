## Analytics

### CPSC 2650

XXX---XXX

## Learning Outcomes

- describe the fields in the Combined Log Format
- describe the limitations of server based logging
- describe some of the features of client side analytics 
- describe some of the limitations of client side analytics
- implement client side analytics

XXX---XXX

## Combined Log Format

- client IP address
- [not very interesting]
- HTTP user ID
- date and time
- request line
- status code 
- size of response
- referer
- user agent

XXX---XXX

## Client IP address

- IP address of host connecting to the web server
- usually this is the end user's IP address
- sometimes this is the IP address of the last proxy server

XXX---XXX

## HTTP user ID

- the user ID if the request used HTTP authentication

XXX---XXX

## Date / Time 

- date and time when processiong of the request was completed

XXX---XXX

## Request line

- as sent from the client 
- method path protocol format 
- e.g. "GET /foo/page1.html HTTP/1.1"

XXX---XXX

## Status Code 

- numeric status code of response
- 200 (OK)
- 404 (not found)
- 301 (permanent redirect)
- etc.

XXX---XXX

## Size of response

- in bytes

XXX---XXX

## Referer 

- URL of page linked from or embedding current request
- essential to the continued functioniong of the surveilance economy

XXX---XXX

## User Agent 

- a string identifying the client software that made the request
- e.g. Chrome, Firefox, etc. or a crawler

XXX---XXX

## Limitations of Server Based Logging

XXX---XXX

## Features of Client Side Analytics

XXX---XXX

## Limitations of Client Side Analytics
