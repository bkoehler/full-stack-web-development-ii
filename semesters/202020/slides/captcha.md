## CAPTCHA

### CPSC 2650

XXX---XXX

## Learning Outcomes

- outline the problems associated with bots
- describe some alternatives to discouraging bots
- describe the diffent types CAPTCHAs
- use a CAPTCHA service in a web application

XXX---XXX

## What is a bot?

XXX---XXX

## What are some good bots?

XXX---XXX

## What are some bad bots?

XXX---XXX

## What are some ways to discourage bots?

XXX---XXX

## What does CAPTCHA stand for?

XXX---XXX

## What are the different types of CAPTCHA?

- math problem
- word problem
- sign in
- read distorted characters

XXX---XXX

## What are the different types of CAPTCHA? (cont.)

- image classification
- audio classification
- honey pot
- behavioural
- proof-of-work

XXX---XXX

## Math Problem

- form contains a field where the user answers
    a math question
- will deter most basic bots
- discriminates against the innumerate?

XXX---XXX

## Word problem

- form contains a field where the user answers
    a word problem
- will deter most basic bots
- can natural language processing defeat?

XXX---XXX

## Sign in

- may be too high a barrier
- user may not want to link with external entity provider 

XXX---XXX

## Distorted characters

- user has to transcribe an image containing distorted characters
- becoming increasing difficult as image processing/OCR improves
- oldest CAPTCHA
- fun fact: originally used to help transcribe scanned texts

XXX---XXX

## Image Classification

- fallback when user fails simpler text
- also becomming increasingly difficult
- a12y for the visually impaired?

XXX---XXX

## Audio Classification

- user has to transcribe an audio clip
- impact of audio recognition services?
- a12y for hearing impaired?

XXX---XXX

## Honey Pot

- hidden (CSS) fields in a form that humans
    don't normally fill out but bots do
- if it is filled out, app assumes it was a bot

XXX---XXX

## Proof-of-Work

- when submitting a form, JavaScript on client
    has to solve a hard math problem and
    include the result with the form submission
- does not require any user interaction
- slows down form submission

XXX---XXX

## Behavioural 

- on page/site  behavior is measured and sent with
    form submission
- server decides what is human and what isn't 
- can be combined with cross-site behaviour
    which is difficult to fake

XXX---XXX

## Project
