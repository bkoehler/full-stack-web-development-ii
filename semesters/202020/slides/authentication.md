## Authentication

### CPSC 2650

XXX---XXX

## Learning Outcomes

- explain why authentication is required
- distinguish between authentication and authorization
- HTTP authentication (basic, digest)
- form-based
- the role of cookies / local storage
- SAML
- OAuth
- authentication in Feahters

XXX---XXX

## Why is Authentication Required?

XXX---XXX

## What is the difference between Authentication and Authorization?

XXX---XXX

## HTTP Authentication

- triggered by a "401 Unauthorized" response
- "WWW-Authenticate" header signals what the server is looking
- client repeats the request with an "Authorization" header
- most common forms are "Basic" and "Digest"
- client typically caches credentials for subsequent requests

XXX---XXX

## HTTP Basic Authentication

- username / password sent to server
- only base64 encoded
- only viable over TLS connections

XXX---XXX

## HTTP Digest Authentication

- server initiates with a "nonce"
- client sends MD5(username,password,nonce)
- full password never sent
- can work without TLS but vulneralbe to MITM attack

XXX---XXX

## Form-based Authentication

- extremely common
- username / password sent to server
- only viable over TLS connections
- typically combined with cookies or local storage
    so as to not have to re-authenticate 
    every request

XXX---XXX

## Cookies 

- small amount of data passed back and forth between
    client and server
- frequently opaque
- only valid for originating domain / page
- expires after a certain amount of time or
    can be discarded by client
- passed using "Cookie" and "Set-cookie" HTTP headers

XXX---XXX

## Browser Local Storage

- browsers have "sessionStorage" and "localStorage" objects
- key/value stores
- partitioned by origin domain name
- "sessionStorage" persists for the duration of the page session 
- "localStorage" persists forever

XXX---XXX

## Problem: managing credentials is a pain!

- have to be kept secure
- user forgets
- user wants to change

XXX---XXX

## Solution: SAML and OAuth

XXX---XXX

## SAML

- Securiy Assertion Markup Language
- XML-based
- Identity Provider (IdP) stores credentials
- Service Provider (SP) asks IdP to authenticate users

XXX---XXX

## OAuth

- Open Authorization
- JSON-based
- Authorization Server stores and validates credentials
    and issues tokens
- Resource Server is capable of using tokens to 
    access protected resources
- does not provide authentication directly
- OpenID Connect is the layer on top that provides authentication

XXX---XXX

## JWT

- JSON Web Token
- header+payload+signature
- header: metadata about payload and signature
- payload: claims
- signature: ensures header and payload can't be tampered with
