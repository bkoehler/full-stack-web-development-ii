## WebSocket API

### CPSC 2650

XXX---XXX

## Learning Outcomes

- 

XXX---XXX

## Network Layers

- HTTP
- TCP
- IP
- physical

XXX---XXX

## TCP Handshake

```text
      Client                                Server
            SYN --->
                                <--- ACK/SYN
            ACK ---> 
```

XXX---XXX

## TCP Teardown

```text
      Client                                Server
            FIN --->
                                    <--- ACK
                                    <--- FIN
            ACK ---> 
```

XXX---XXX

## HTTP 0.9 (and 1.0)

```text
      Client                                Server
                    {TCP Handshake}
            HTTP Request --->
                          <--- HTTP response
                     {TCP Teardown}
```

XXX---XXX

## HTTP 0.9 (and 1.0)

```text
      Client                                Server
                    {TCP Handshake}
            HTTP Request --->
                          <--- HTTP response
                     {TCP Teardown}
                    {TCP Handshake}
            HTTP Request --->
                          <--- HTTP response
                     {TCP Teardown}
                    {TCP Handshake}
            HTTP Request --->
                          <--- HTTP response
                     {TCP Teardown}
```

XXX---XXX

## HTTP 1.1

```text
      Client                                Server
                    {TCP Handshake}
            HTTP Request --->
                          <--- HTTP response
            HTTP Request --->
                          <--- HTTP response
            HTTP Request --->
                          <--- HTTP response
                     {TCP Teardown}
```

XXX---XXX

## HTTP Polling

- client always initiates communication
- server cannont initiate communication
- solutions using HTTP
    - short polling
    - long polling

XXX---XXX

## Short Polling

```text
      Client                                Server
                    {TCP Handshake}
            HTTP Request (somehting new?) --->
                     <--- HTTP response (no)
            HTTP Request  (somehting new?) --->
                     <--- HTTP response (no)
            HTTP Request  (somehting new?) --->
                   <--- HTTP response (yes!)
                     {TCP Teardown}
```

XXX---XXX

## Long Polling

```text
      Client                                Server
                    {TCP Handshake}
            HTTP Request  (somehting new?) --->
                        (wait for something to happen)


                    <--- HTTP response (yes!)
            HTTP Request  (somehting new?) --->



                    <--- HTTP response (yes!)
                     {TCP Teardown}
```

XXX---XXX

## Alternative to Polling

- WebSocket 

XXX---XXX

## WebSocket Handshake Request

```text
GET /chat HTTP/1.1
Host: example.com:8000
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
Sec-WebSocket-Version: 13
```

XXX---XXX

## WebSocket Handshake Response

```text
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=
```

XXX---XXX

## WebSocket Data Frame

```text
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-------+-+-------------+-------------------------------+
|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
|N|V|V|V|       |S|             |   (if payload len==126/127)   |
| |1|2|3|       |K|             |                               |
+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
|     Extended payload length continued, if payload len == 127  |
+ - - - - - - - - - - - - - - - +-------------------------------+
|                               |Masking-key, if MASK set to 1  |
+-------------------------------+-------------------------------+
| Masking-key (continued)       |          Payload Data         |
+-------------------------------- - - - - - - - - - - - - - - - +
:                     Payload Data continued ...                :
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|                     Payload Data continued ...                |
+---------------------------------------------------------------+
```

XXX---XXX

## Complete WebSocket Session

```text
      Client                                Server
                    {TCP Handshake}
            HTTP WS handshake Request --->
             <--- HTTP WS handshake response
                                  <--- Frame
                                  <--- Frame
            Frame --->
            Frame --->            <--- Frame
            Frame --->            <--- Frame
            Frame --->            <--- Frame
                                  <--- Frame
                                  <--- Frame
            Frame --->
                     {TCP Teardown}
```

XXX---XXX

## Socket.IO

- realtime client/server library for JavaScript
- uses WebSocket or HTTP long polling as appropriate
- gracefully detects and handles disconnection or reconnection
- can group connections into "rooms"
- can be used as a transport for Feathers
