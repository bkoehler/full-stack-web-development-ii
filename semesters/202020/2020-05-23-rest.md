---
layout: page
title:  "REST"
date:   2020-05-23 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- describe the essential features of a RESTful API
- utilize the Feathers framework to 
    implement a RESTful API for an application
- incorporate a database (MongoDB) into a
    web application

## Resources

- [Understanding and Using REST 
    APIs](https://www.smashingmagazine.com/2018/01/understanding-using-rest-api/){:target="_blank"}
- [Best Practices for Designing a Pragmatic RESTful 
    API](https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api){:target="_blank"}
- [MongoDB](https://www.mongodb.com/){:target="_blank"}
- [mongoose](https://mongoosejs.com/){:target="_blank"}
- [Feathers](https://feathersjs.com/){:target="_blank"}
- [slides](slides/rest.html){:target="_blank"}

## Lab

[Video walk through of this lab](https://youtu.be/7fwZImJB11U).

Fork this project: `https://gitlab.com/langarabrian/rest2`.

Remove the fork relationship, make the project
private, and clone YOUR COPY of the
project into your Cloud Shell environment.

Change to the project directory (e.g. `cd rest2`).

Install the dependencies and start
the application:

{% highlight shell %}
npm install
PORT=8080 npm run dev
{% endhighlight %}

Preview the running application and verify that the signup page
appears in the browser.

Stop the application with Ctrl-C.

### Create REST Service Backend

Install the feathers CLI:

```shell
npm i -g @feathersjs/cli
```

Create a new service for the application to manage "signups":

{% highlight shell %}
feathers generate service
{% endhighlight %}

Answer the questions as follows:

```
What kind of service is it? Mongoose
What is the name of the service? signups
Which path should the service be registered on? /signups
What is the database connection string? MONGODBURI
```

`MONGODBURI` is an environment variable containing
the MongoDB connection string. You will have
to set this before running the application, e.g.

```shell
export MONGODBURI='mongodb+srv://...'
```

The generated file, `src/mongoose.js`, is missing a connection
setting. Add `useUnifiedTopology: true` to the list of 
mongodb connection settings.

In your editor, open up `src/models/signups.model.ts`.

There is currently a Mongoose schema definition for signups that
looks something like this:

{% highlight typescript %}
  const schema = new Schema({
    text: { type: String, required: true }
  }, {
    timestamps: true
  });
{% endhighlight %}

Edit it so that it looks like this:

{% highlight typescript %}
  const schema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    country: { type: String, required: true },
    province: { type: String, required: true },
    postalCode: { type: String, required: true }
  }, {
    timestamps: true
  });

{% endhighlight %}

### Create REST Client Frontend

In `public/index.html` add the following lines toward the end of the file
after `bootstrap.min.js` but before `form-validation.js`:

{% highlight html %}
<script type="text/javascript" 
        src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>
<script src="//unpkg.com/@feathersjs/client@^3.0.0/dist/feathers.js"></script>
{% endhighlight %}

In `public/form-validation.js` add the following code at the beginning of
the `load` event listener to set up the `signups` service in the client:

{% highlight javascript %}
    // Set up FeathersJS app
    var app = feathers();
    
    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client 
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `signups` service
    const signups = app.service('signups');
{% endhighlight %}

A little further down in `public/form-validation.js` there is code
to validate the form fields and nothing else:

{% highlight javascript %}
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
{% endhighlight %}

Modify it as follows to use the `signups` service to
create a new signup when the form is valid:

{% highlight javascript %}
      form.addEventListener('submit', function (event) {
        if (form.checkValidity()) {
          signups.create({
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            email: $('#email').val(),
            country: $('#country').val(),
            province: $('#province').val(),
            postalCode: $('#postalCode').val()
          });
          form.classList.remove('was-validated');
          form.reset();
        } else {
          form.classList.add('was-validated');
        }
        event.preventDefault();
        event.stopPropagation();
      }, false);
{% endhighlight %}

Re-start the backend:

{% highlight shell %}
PORT=8080 npm run dev
{% endhighlight %}

Preview the applicaition and load in its own browser tab.

Fill out the form and submit.

You can verify that the data got added to the MongoDB database
by looking at the collection in the MongoDB Atlas console.

### Read Data from REST Service

Next, we will modify the client-side app so that it displays all the
current signups in the table at the bottom of the page.

First, at the top of `public/form_validation.js` add a function
at the very top of the file that adds a single row to the table:

{% highlight javascript %}
// Adds a signup row to the table
const addSignup = signup => {
  $('#signups > tbody:last-child').append(
    `<tr>
      <td>${signup.firstName}</td>
      <td>${signup.lastName}</td>
      <td>${signup.email}</td>
      <td>${signup.country}</td>
      <td>${signup.province}</td>
      <td>${signup.postalCode}</td>
    </tr>`
  );
};
{% endhighlight %}

Then, add a function that will fetch the all of the signups
from the server and then add them one at a time to the table:

{% highlight javascript %}
// Shows the signups
const showSignups = async signupService => {
  // Find the latest 25 signups. They will come with the newest first
  const signups = await signupService.find({
    query: {
      $sort: { createdAt: -1 },
      $limit: 25
    }
  });
  
  // We want to show the newest signup last
  signups.data.reverse().forEach(addSignup);
};
{% endhighlight %}

Finally, call the `showSignups` function on page load.
In the `load` event listener, add the following code
just after the connection to the `signups` service
has been established.

{% highlight javascript %}
    // Show existing signups in the table
    showSignups(signups);
{% endhighlight %}


## Assignment

1. Add the required code to `public/form_validation.js` to
    immediately add the form data to the table at the bottom
    of the page.
1. Add the required code to `public/form_validation.js` and
    `public/index.html` to display a "Delete" button at the
    end of each row in the table. When the "Delete" button
    is clicked, the corresponding row should be deleted from
    the table and you should invoke the correct Feathers
    service method to delete the document from the
    MongoDB collection.
1. Implement CI/CD for the project so that the application
    will be deployed to Google Cloud Run.
1. Stage, commit, and push the changes.
