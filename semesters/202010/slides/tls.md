## TLS

### CPSC 2650

XXX---XXX

## Learning Outcomes

- explain why encryption is required
- describe how encryption over the web works
- explain how the TLS handshake works
- describe the role of certificates and certificate authorities
- describe the differences between DV, OV, and EV certificates
- get a certificate for a domain
- deploy a certificate for a web server

XXX---XXX

## Why is encryption required?

XXX---XXX

## Why is encryption required?

- privacy
- integrity

XXX---XXX

## Cryptography Primer

- symetric key cryptosystems
- public key cryptosystems
- digital signatures

XXX---XXX

##  Symetric Key Cryptography

- parties share a secret key
- secret key is used for both encryption and decryption
- parties have to agree on key out-of-band
- fast and secure


XXX---XXX

## Public Key Cryptography 

- key has a public and private parts
- public part can be shared freely
- private key must be kept secret
- encrypt with one key; decrypt with the other

XXX---XXX

## Digital signatures

- used to confirm that a digital document has not been tampered with
- usually a hash of the documet encrypted with the authors private key
- can be verified by anyone using authors public key

XXX---XXX

## Problem

- How do we know an author's public key is legitimate?
- Solution: have author's public key signed by a well-known
    certificate authority (CA)

XXX---XXX

## Implementation on the web

- client asks server for a certificate with server's public key
- client verifies certificate
- client generates random symetric key
- client encrypts generated key with server's public key
- client sends message to server
- server decrypts message with own private key
- both parties now have a copy of the symetric key which they can use

XXX---XXX

## Certificate Validation

- browsers have a handfull of "root" keys built-in
- TLS certs usually have chain of signatures leading to a root key
- in this way a browser can validate an arbitrary certificate provided
    there is a chain to one of it's roots

XXX---XXX

## Classes of certificates

- Domain Validated (DV)
- Organization Validated (OV)
- Extended Validation (EV)

XXX---XXX

## Domain Validated (DV)

- CA only checks that domain is controlled by requestor
- usually an automated process

XXX---XXX

## Organiztion Validated (OV)

- CA checks out the organization that contols a domain
- usually a manual process
- supposed to provide some confidence about what organization a user is connecting with

XXX---XXX

## Extended Validation (EV)

- supposedly more extensive checking than OV?
- used to be signalled in the browser with organization name 
- this no longer the case
- today the value of EV is unclear

XXX---XXX

## Delploying TLS

- generate public and private keys
- prove to CA that you control domain
- CA signs certificate containing your public key
- configure web server to know about private key and certificate
