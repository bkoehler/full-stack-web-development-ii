## REST

### CPSC 2650

XXX---XXX

## Learning Outcomes

- distinguish between traditonal and contemporary
    web application architectures
- describe the key features of a REST API 
- use a framework like Feathers to develop REST APIs

XXX---XXX

## Traditonal Web Application Architecutre

- mostly GET and POST requests
- responses are entire complete web pages
- every request causes the browser to
    reload a complete page 
- light on client-side JavaScript

XXX---XXX

## Contemporary Web Application Architecutre

- GET/POST/PUT/PATCH/DELETE methods
- responses are (small) JSON objects
- application is a single page
- heavy use of XMLHTTPRequest API
- heavy client-side JavaScript

XXX---XXX

## REST Concepts

- endpoint
- service / resource
- methods (CRUD)

XXX---XXX

## REST Endpoint

- domain name from which the API is available
- HTTPS everywhere
- typically separate from static web server for application

XXX---XXX

## REST Service / Resource

- named path representing either a collection of objects
    or a provied service
- e.g. users, messages, signups, meetings, etc.
- a single endpoint may manage multiple services or resources

XXX---XXX

## REST - Create

- e.g. POST /messages
- request body contains JSON encoding of
    new object to be created
- response contains copy of object, possibly
    augmented with unique identifier

XXX---XXX

## REST - Read (many)

- e.g. GET /messages?...
- retrieves a collection of resources that
    matches the specified query

XXX---XXX

## REST - Read (one)

- e.g. GET /messages/a1cde873
- retrieves a single resource matching the 
    given identifier

XXX---XXX

## REST - Update (PUT)

- e.g. PUT /messages/a1cde873
- _replaces_ an existing resource with the
    contents of the request body
- returns the new resource

XXX---XXX

## REST - Update (PATCH)

- e.g. PUT /messages/a1cde873
- _merges_ an existing resource with the
    contents of the request body
- returns the resulting merged resource

XXX---XXX

## REST - Delete

- e.g. DELETE /messages/a1cde873
- deletes an existing resource
- returns the deleted resource

XXX---XXX

## Complete Application Architecutre

- client (browser)
- static site web server (e.g. www.myapp.com)
- REST API server (e.g. api.myapp.com)
- in-memory datastore (e.g. Redis)
- persistent datastore (e.g. MongoDB)

XXX---XXX

## Feathers Highlights

- framework for building RESTful APIs
- generates Express app 
- generates all CRUD methods / routes
- has adapters for popular databases
- key concepts are service and hooks
- also supports authentication

XXX---XXX

## Feathers Services

- a service is a class that implements
    a prescibed interface
- can be backed by a database adapter (or not)
- methods are: find, get, create, update, patch, and remove
- methods can be invoked directly or linked to a transport
    (e.g. REST, WebSocket, etc.)

XXX---XXX

## Feathers Hooks 

- functions tied to core service methods
- can be executed BEFORE, AFTER, or on ERROR with
    repect to the underlying service method 
- multiple hooks can be chained together to
    create more complex functionality

XXX---XXX



XXX---XXX



XXX---XXX



XXX---XXX



XXX---XXX



XXX---XXX

