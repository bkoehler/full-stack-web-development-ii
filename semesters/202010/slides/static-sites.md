## Static Sites

### CPSC 2650

XXX---XXX

## Learning Outcomes

- deploy the frontend for a web application to a static web host

XXX---XXX

## Frontend Development Servers 

- React: `yarn start`
- Angualar: `ng serve`
- don't use in production!

XXX---XXX

## Building Static Frontends

- React: `yarn build`
- Angualar: `ng build --prod`

XXX---XXX

## Deploying Static Frontend: Easy

- copy static frontend into "public"
    directory of Feathers backend
- not the most efficient way to serve static assets

XXX---XXX

## Deploying Static Frontend: Alternatives

- use a dedicated static web host:
    - Firebase
    - AWS S3 + CloudFront
    - GitLab Pages
    - GitHub Pages

XXX---XXX

## Firebase

- comes with global content delivery network (CDN)
- custom domain and TLS: easy 
- only pay for storage and bandwidth

XXX---XXX

## AWS S3 + CloudFront

- S3: static storage
- CloudFront: CDN
- custom domain and TLS: more complicated 
- only pay for storage and bandwidth
- cheaper for very large applications

XXX---XXX

## GitLab Pages

- custom domain and TLS: easy
- free for "reasonable" use
- no upgrade path to pay for heavy use

XXX---XXX

## GitHub Pages

- similar to GitLab pages
