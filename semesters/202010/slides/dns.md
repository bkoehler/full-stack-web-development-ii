## DNS

### CPSC 2650

XXX---XXX

## Learning Outcomes

- explain the need for DNS
- describe the differences between the various TLDs
- describe the role of the following in the DNS: registry, registrar, and registrant
- explain the role of the following types of name servers:
    - root
    - top-level
    - authoritative
    - recursive (or local resolvers)

XXX---XXX

## Learning Outcomes continued

- register a second level domain
- identify typical domain name disputes
- configure resource records (RR) of the following types for an application: SOA, A, CNAME, MX, NS, TXT, AAAA
- retrieve DNS information using `dig`  

XXX---XXX

## Why DNS?

XXX---XXX

## Why DNS?

- entities need control over their own name space
- need something to attach TLS certificates to

XXX---XXX

## TLDs

- original generics
- ISO cc 
- new generics
- vanity TLDs

XXX---XXX

## Original Generic TLDs

- com, org, net, int, edu, gov, and mil
- orignally all restricted
- com, org, and net are all open registration today 

XXX---XXX

## ISO CC TLDs

- International Standards Organization Country Code
- two character TLDs based on ISO 3166-1 alpha-2 codes
- there are a few exceptions
- authority to register typically delegated to an 
    appropriate national body
- rules differ; some are open; some require country presence

XXX---XXX

## New Generic TLDs

- aero, biz, coop, info, museum, name, and pro approved in 1998
- steady stream since
- there is a formal process for approving new TLDs

XXX---XXX

## Vanity TLDs

- not for the faint of heart
- must demonstrate technical capacity to operate registry
- US$185,000 application fee
- may not be approved
- if successful, US$6,250 per quarter ongoing

XXX---XXX

## DNS Participants

- registry
- registrar
- registrant

XXX---XXX

## Registry 

- single organization with responsibility for
    maintaining list of registed second-level
    domains
- operates top-level domain name server for
    its domain
- funded by per domain fees

XXX---XXX

## Registrar 

- organization that registers second-level domain
    names on behalf of registrants
- go-between for registry and registrants
- usually multiple registrars for each TLD
- competitive market with registrars free to
    mark-up (or discount) registry's domain fee

XXX---XXX

## Registrant 

- individual or organization that wishes to control
    a given second-level domain
- registers second-level domain through registrar
- a registrant retains control as long the annual fees
    are paid

XXX---XXX

## Name servers

- root
- top-level
- authoritative
- recursive (local resolvers)

XXX---XXX

## Root Name Servers 

- geograpically distributed
- operated by different organizations
- maintain definitive list of top-level
    domain name servers

XXX---XXX

## Top Level Name Servers

- operated by registry for a given TLD
- typically geographically distributed
- maintain definitive list of authoritative
    name servers for registered second-level
    domains

XXX---XXX

## Authoritative Name Servers

- operated by registrant or delegate (typically registrar)
- responds to requests for DNS resolution below
    a given second-level domain

XXX---XXX

## Recursive (or local resolvers)

- provide DNS resolultion services for end users
- typically operated by ISPs
- maintain a cache of most frequently requested resource records (RR)
- entries expire from cache when the RR TTL is up

XXX---XXX

## Resource Record Types 

- SOA
- A, AAAA
- CNAME
- NS
- MX
- TXT

XXX---XXX

## SOA Record

- Start Of Authority 
- fields
    - master name server
    - email address of administrator
    - serial number
    - refresh
    - retry 
    - expire
    - TTL for negative responses
.

XXX---XXX
 
## A and AAAA Records 

- IPv4 address (A)
- IPv6 address (AAAA)

XXX---XXX

## CNAME Record

- Canonical Name 
- creates an alias from one domain to another (the
canonical name)


XXX---XXX

## MX Record

- Mail eXchanger
- domain name of host that will handle email for this domain
- can be multiple records of this type with different priorities
- if no MX record, host pointed to by A (or AAAA) record for
    second-level domain name is used

XXX---XXX

## TXT Record 

- TeXT
- used for arbitrary text 
- frequently used for domain validation or spam filtering (SPF/DKIM)

XXX---XXX

## Domain Name Disputes

XXX---XXX

## plannedparenthood.com

XXX---XXX

## peta.org

XXX---XXX

## fuckgeneralmotors.com

XXX---XXX

## whitehouse.com
