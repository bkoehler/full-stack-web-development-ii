---
layout: page
title:  "Analytics"
date:   2020-01-13 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- describe the motiviation for collecting analytics for a web application
- describe the options available for collecting analytics
- implement Google Analytics for a web application

## Resources
 
- [slides](slides/analytics.html){:target="_blank"}

## Lab

This lab is a continuation of the previous email lab which
we augment by adding Google Analytics to the application
enabling page and event tracking.

### Create a Google Analytics Account and Property

Login into your Google account if you have not already done so.

Go to [Google
Analytics](https://analytics.google.com/analytics/web){:target="_blank"}.
Click on the "Start measuring" or "Set up for free" or whatever the button
says.

Create an account name. Agree to data sharing, or not, as you see fit.
Click on the "Next" button.

For "What do you want to measure", choose "Web". Click on the "Next" button.

Under "Property details", enter whatever you want for "Website name".

For "Website URL", choose "http://app1.4949NN.xyz".

Select whatever you want for "Industry Category".

Make the "Reporting Time Zone" "Canada/Vancouver Time".

Click on the "Create" button.

Agree to the terms and conditions.

You should be taken to a page that shows your Tracking ID and code.

### Modify the Frontend to do Page Tracking

In the terminal where the frontend is runnig, stop it (Ctrl-C).

Add the `history` and `react-ga` modules to the project:

{% highlight shell %}
yarn add history react-ga
{% endhighlight %}

Import the modules into the application in `frontend/src/application.js`:

{% highlight javascript %}
import ReactGA from 'react-ga';
import { createBrowserHistory } from 'history';
{% endhighlight %}

Just before the definiton of the `Application` class, initialize
ReactGA, set up
the history object, and the history event listener:

{% highlight javascript %}
// initialize ReactGA
const trackingId = "UA-1234567890-1"; // Replace with your Google Analytics tracking ID
ReactGA.initialize(trackingId);

// set up history
const history = createBrowserHistory();

// Initialize google analytics page view tracking
history.listen(location => {
  ReactGA.pageview(location.pathname); // Record a pageview for the given page
});
{% endhighlight %}

Finally, add the `history` event listener to the `Router` component:

{% highlight javascript %}
    <Router history={history}>
{% endhighlight %}

Restart the frontend and flip between frontend pages in the browser.
You should see the results in the Realtime Overview of the Google
Analytics dashboard.

### Modify the Frontend to do Event Tracking

Import ReactGA into `frontend/src/emailform.js`:

{% highlight javascript %}
import ReactGA from 'react-ga';
{% endhighlight %}

In the `sendEmail` event handler, record an email send event:

{% highlight javascript %}
    ReactGA.event({
      category: "Email",
      action: "Send",
    });
{% endhighlight %}

Reload the application in the browser. Send an email. You 
should see the results in the Realtime Events of the Google
Analytics dashboard.

## Assignment

1. TBD.
