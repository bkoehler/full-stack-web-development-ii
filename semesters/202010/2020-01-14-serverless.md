---
layout: page
title:  "Serverless"
date:   2020-01-14 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- outline the problems associated with scaling the backend of
    a web application
- implenent a serverless web application using 

## Resources
 
- [The Rise of Serverless 
Computing](https://cacm.acm.org/magazines/2019/12/241054-the-rise-of-serverless-computing/fulltext){:target="_blank"}
- [What are Cloud Computing 
Services](https://medium.com/@nnilesh7756/what-are-cloud-computing-services-iaas-caas-paas-faas-saas-ac0f6022d36e){:target="_blank"}
- [slides](slides/serverless.html){:target="_blank"}

## Lab

### Set Up the Database

Get to the AWS Console.

Go to "Services \| Database \| DynamoDB".

Click on the button to "Create Table".

For table name, enter "books".

For primary key, enter "isbn", and set the type to "Number".

Click on the "Create" button at the bottom right. Wait for the table to be created.

Click on the "Items" tab.

Click on the "Create item" button.

For "isbn", enter a number.

Append a "String" property for "title" and enter a value.

Append a "Number" property for "pages" and enter a value.

Click on the "Save" button.

### Set Up the Lambda Function

In a new browser tab, go to the AWS Console.

Go to "Services \| Compute \| Lambda".

Click on the "Create function" button.

Choose "Author from scratch".

For "Function name", enter "library".

Click on the "Create funtion" button.

In the "Function code" region, replace the default function
with the following:

{% highlight javascript %}
var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "books";

// need for cross-site POST to work
const corsHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'content-type'
};

async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    let params = {
        TableName: table,
        Item: book
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

exports.handler = async (event) => {
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body );
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/books') {
        return newBook(body);
    }
    
    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};
{% endhighlight %}

Save the changes (the big "Save" button in the main page).

### Set Up the API Gateway

Still on the Lambda function page, in the "Designer"
region, click on the "Add trigger" button.

Select the "API Gateway" trigger.

Select "Create a new API".

For template, choose "HTTP API".

Click the "Add" button.

You will be returned to the Lambda function page. The newly created
API Gateway will be highlighted. Click on the name of the API Gateway,
"library-API".

Click on "Routes" under "Develop".

Click on "Create" to create a new route.

For the method and route, choose "ANY" and "/library/books".

Click on the "Create" button.

In the route summary, click on the "ANY" under "/books".

Click on the "Attach integration" button.

Select the existing "library" integration and click the "Attach integration" button.

### Allow the Lambda Function to Access the Database

In a new browser tab, go to the AWS Console.

Go to "Services \| Security, Identity, & Compliance \| IAM".

Click on "Roles". Find and click on the role for your Lambda function.

Under "Permissions", click on the "Attach policies" button.

Search for "DynamoDB", check "AmazonDynamoDBFullAccess", and click
the "Attach policy" button.

### Set Up the frontend

In another tab, get into your Cloud9 environment.

In another tab, sign in to GitLab and go to this
project page: `https://gitlab.com/langarabrian/serverless1`.

Fork this project; remove the fork relationship; and clone in
your Cloud9 environment.

Change to the project directory (e.g. `cd serverless1`).

Start the frontend container:

{% highlight shell %}
scripts/start-frontend-container.sh
{% endhighlight %}

In the container terminal, install the dependencies
and start the React development server:

{% highlight shell %}
yarn install
yarn start
{% endhighlight %}

In the Cloud9 IDE, "Preview running application", move to its own
tab.

*** something about web socket security here???? ***

Test out the add book form (only works locally right now).

The last step is to integrate fronted with the API Gateway.

First, import the `axios` module into `src/books.js` and setup
the base URL for the API:

{% highlight javascript %}
import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://YOUR-ENDPOINT.execute-api.us-east-1.amazonaws.com/default/library";
{% endhighlight %}

And then add the code in the `addBook()` function to call the remote API:

{% highlight javascript %}
    let newbook = {
      isbn,
      title,
      pages
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      console.log(res);
    });
    
    ev.preventDefault();
{% endhighlight %}

Save the code. Refresh the application in the browser. Add a new book.
Check the items in the DynamodDB console to make sure that it appeared.

## Assignment

1. Modify the frontend and the Lambda function so it displays
    all the books in the database in the table at the bottom
    of the page.
1. Modify the frontend and the Lambda function so that books
    can be deleted.
1. Implement a separate database and frontend page for authors.
    Create fields for author id (aid), first name (fname), and
    last name (lname).
1. Modify the books page so that there is a new field for the
    author of the book. When entering a new book, show a pick list
    for author based on the contents of the authors database. The
    books table should store author id (aid), but the frontend
    should show the corresponding author's first and last name. 
