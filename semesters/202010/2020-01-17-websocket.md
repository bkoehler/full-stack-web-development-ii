---
layout: page
title:  "WebSocket API"
date:   2020-01-17 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- describe the limitations of HTTP for real-time applications
- use polling to address those shortcomings
- implement a real-time application using the WebSocket API

## Resources

- [An Introduction to WebSockets](https://blog.teamtreehouse.com/an-introduction-to-websockets){:target="_blank"}
- [slides](slides/websocket.html){:target="_blank"}

## Lab

1. Fork; remove fork relationship; and clone this project:
    `https://gitlab.com/langarabrian/websocket1`
1. Walk through `simple` chat application.
1. Walk through `fancy` chat application.

## Assignment

1. Modify the `fancy` chat application so that in addition to
    a nickname, the use can choose a "room" from a drop-down
    list of pre-defined rooms. After entering, the user only
    sees messages from users that are also in that room.
1. Modify the `fancy` chat application so that there is a way
    for the user to leave the room (other than closing the tab!)
    Upon leaving the room, the user should again be able to
    enter a new nickname and choose a new room.
