---
layout: page
title:  "Angular"
date:   2020-01-19 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- create simple Angular frontend applications

## Resources

- [Intro to Basic Concepts](https://angular.io/guide/architecture){:target="_blank"}
- [Intro to Modules](https://angular.io/guide/architecture-modules){:target="_blank"}
- [Intro to Components](https://angular.io/guide/architecture-components){:target="_blank"}
- [Intro to Services and DI](https://angular.io/guide/architecture-services){:target="_blank"}

## Lab

Read about the fundamental Angular concepts above.

I have updated the `bkoehler/feathersjs` image with the Angular CLI.
Be sure to delete the old one as follows before continuing with
the rest of exercise:

```shell
docker rmi bkoehler/feathersjs
```

Fork; remove the fork relationship; and clone this project
`https://gitlab.com/langarabrian/toh`

Change to the project directory: `cd toh`

Start the frontend container: `scripts/start-frontend-container.sh`

Install the dependencies and run the development server:

```shell
npm install 
ng serve 
```

Test that application is working by using the Cloud9
**Preview Running Application** menu item. Move the preview 
to its own browser tab.

Complete the [Angular Tour of Heroes app and 
tutorial](https://angular.io/tutorial){:target="_blank"}.
Readd the IMPORTANT NOTES below first.

### IMPORTANT NOTES

1. Read the [Introduction](https://angular.io/tutorial){:target="_blank"}
section.

1. You can just read the [Create a Project](https://angular.io/tutorial/toh-pt0){:target="_blank"}
section. Any steps in that section are already part of the project that
you cloned.

1. You can start actually following the steps from the [1. The Hero
Edition](https://angular.io/tutorial/toh-pt1){:target="_blank"} section.

1. If `ng serve` is running and you have to enter another `ng` command,
you will have to stop `ng serve` first (CTRL-C); run the command; and then
restart the development server with `ng serve`. 

### Clean up 

Stop `ng serve` (CTRL-C). Stop the container (CTRL-D).

### Assignment Preparation

Change back to the `environment` directory (e.g. `cd ~/environment`).

Fork; remove the fork relationship; and clone this project
`https://gitlab.com/langarabrian/angular-cars`

Change to the project directory (e.g. `cd angular-cars`).

Edit `etc/nginx/conf.d/backend.conf` so that it uses your backend domain name.

Edit `etc/nginx/conf.d/frontend.conf` so that it uses your frontend domain name.

Edit `frontend/src/app/services/feathers.service.ts` so
that it uses your backend domain name.

Start the backend container:

```shell
scripts/start-backend-container.sh
```

Install the dependencies for the backend and start it:

```shell
npm install
npm run dev 
```

In another terminal change the `angular-cars` project directory.
Start the frontend container:

```shell
scripts/start-frontend-container.sh
```

Install the dependencies for the frontend and start it:

```shell
npm install
ng serve 
```

In another terminal change the `angular-cars` project directory.
Start the NGINX container:

```shell
scripts/start-nginx-container.sh -it
```

In a new browser tab navigate the frontend URL. Follow the "Things"
link. Add some new things to confirm that the applicaiton is working.

Continue with the assignment below. No changes are required to
backend. It already implements the "things" and "cars" service.

## Assignment

1. Create a new component "cars".
1. Modify `frontend/src/app/app-routing.module.ts` with an 
additional route for the "cars" path that will display
"CarsComponent".
1. Modify `frontend/src/app/app.component.html` with a link
"Cars" to the new component.
1. Modify the "cars" component so that it implents the following
functionality:
    - displays a form that allows the user enter a new car with
    the following attributes: make, model, year, and mileage
    - displays a table with all the current cars in the system
    - in each row of the table, have a "Delete" button so that
    the user can delete a car
1. Bonus: The application is currently unstyled. If you have time,
implement the Bootstrap framework for the application.

### Hints

Spend some time to make sure that you understand how the "things"
component works first. Here are some highlights:

`frontend/src/app/services/feathers.service.ts` initiates the
connection to the Feathers backend. This file should require 
no further modification.

`frontend/src/app/services/data.service.ts` alredy has methods
to return the active list of "things" and to add a new thing. You
will have to modify this class so that it has methods to:
- return the active list of "cars"
- add a new car
- delete a car

`frontend/src/app/things/things.component.ts` defines the
"ThingsComponent" class. This class has methods to
access the "things" data on the backend as well as
handling the form submission. Your "CarsComponent"
will be similar. It will need an addition method 
to handle deleting a car.

`frontend/src/app/things/things.component.html` contains
the HTML template for the component. It has a form
element for adding new things and an unorderd list for
displaying the current list of things. Your template
for the cars component will be similar.


