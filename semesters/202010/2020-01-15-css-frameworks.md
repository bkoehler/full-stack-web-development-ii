---
layout: page
title:  "CSS Frameworks"
date:   2020-01-15 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- use a CSS framework in an application 

## Resources

- [Bootstrap Documentation](https://getbootstrap.com/docs/4.4/getting-started/introduction/){:target="_blank"}
    - [Bootstrap Grid System](https://getbootstrap.com/docs/4.4/layout/grid/){:target="_blank"}
    - [Bootstrap Forms](https://getbootstrap.com/docs/4.4/components/forms/){:target="_blank"}
    - [Bootstrap Form Validation](https://getbootstrap.com/docs/4.4/components/forms/#validation){:target="_blank"}
- [Sample React/Feathers/Bootstrap App](https://gitlab.com/langarabrian/bootstrap-demo){:target="_blank"}
- [slides](slides/css-frameworks.html){:target="_blank"}

## Lab

## Assignment

1. Fork; remove the fork relationship; and clone this project:
    https://gitlab.com/langarabrian/bootstrap1
1. Start the backend, frontend, and NGINX containers.
1. Create a data collection form with the following required fields:
    - first name
    - last name
    - email address
    - country
    - province/state
    - postal/zip code
    - lowest systolic blood pressure in last year
    - highest systolic blood pressure in last year
1. Use the Bootstrap classes to create a sensible responsive
    layout for the input fields. There should be at least
    3 different arrangements depending on the viewport width. 
1. Choose the most restrictive input field type that makes
    sense for each field.
1. Implement "country" as a "SELECT" element where there are two choices:
    "Canada" and "USA".
1. Implement "province/state" as a "SELECT" element where the choices
    depend on the current selection for "country".
1. Implement "postal/zip code" validation that is dependent on the
    current selection for "country". You should support Canadian postal
    codes and US ZIP codes in the basic 5-digit format as well as the
    ZIP+4 format.
1. Ensure that lowest blood pressure entered is lower than the
    highest blood pressure entered.
1. Do as much validation as possible on the client side.
    Display meaningful error messages.
    Short-circuit the form submission if any of the fields are invalid.
1. On the server side, validate all the input in the "before create"
    hook and throw an error
    if any of the input fields are invalid.
1. Also, on the server side, throw an error if someone tries to
    register with an email address that has already been registered.
1. Catch any server side errors on the client side and present
    a meaningful message to the user in the UI.
