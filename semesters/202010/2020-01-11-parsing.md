---
layout: page
title:  "Parsing"
date:   2020-01-11 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- extract information from a document using a parsing library

## Resources

- [slides](slides/parsing.html){:target="_blank"} 

## Lab

Get the number of compute labs from a Langara page.

## Assignment

1. Get the city, date/time, and temperature from a weather.gc.ca page.
