---
layout: page
title:  "Email"
date:   2020-01-12 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- describe the contemporary landscape for email delivery
- send transactional/notification email from an application
    with high deliverability

## Resources

- [slides](slides/email.html){:target="_blank"}

## Lab

### Setup

Start your Cloud9 environment. Go to the EC2 console. Copy the "Public DNS" for your Cloud9 instance.
Go to the Google Cloud Platform "Cloud DNS" tool and create 2 new CNAME records (`api1.4949NN.xyz` and
`app1.4949NN.xyz`) that point to your Cloud9 instance.

Sign into GitLab. Fork this project: `https://gitlab.com/langarabrian/email1`. Remove the fork relationship.
Clone YOUR fork of the project in your Cloud9 environment.

Change to the project directory (e.g. `cd email1`).

Start an interactive container shell for the
backend: `scripts/start-backend-container.sh`

In the backend shell, install and run the Feathers backend:

{% highlight shell %}
npm install
npm run dev
{% endhighlight %}

Start a new terminal. Change to the project directory.
Start an interactive container shell for the
frontend: `scripts/start-frontend-container.sh`

In the backend shell, install and run the React frontend:

{% highlight shell %}
yarn install
yarn start
{% endhighlight %}

Edit the following files with your own domain names
(e.g. `api1.4949NN.xyz` and
`app1.4949NN.xyz`):

{% highlight shell %}
etc/nginx/conf.d/backend.conf
etc/nginx/conf.d/frontend.conf
frontend/src/feathers.js
{% endhighlight %}

Start a new terminal. Change to the project directory.
Start the Nginx 
container: `scripts/start-nginx-container.sh -it`

Test the backend by visiting the following URL: 

`http://api1.4949NN.xyz`

You should see the Feathers logo.

Test the frontend by visiting the following URL: 

`http://app1.4949NN.xyz`

You should see the basic application.

### Adding Email

We are going to modify the application so that it can 
send email notifications or transactional email.

#### Register for a SendGrid Account 

Login into your Google account if you have not already done so.

Go to the [SendGrid marketplace 
page](https://console.cloud.google.com/marketplace/details/sendgrid-app/sendgrid-email){:target="_blank"}.
Click on the "Start with the free plan" button.

On the subscription, choose the free plan, tick the box, and click
the "Subscribe" button.

On the next page, click the "Register with SendGrid" button.

Complete the form ... eventually you should be redirected back to Google marketplace.
The page should indicate that you are subscribed to the free plan.

Follow the button to "Manage API keys on the SendGrid website". Login with
your SendGrid credentials.

#### Authenticate Your Domain for Sending

Under "Settings", choose "Sender Authentication".

Click on the "Authenticate Your Domain" button. For "DNS Host", choose "Google
Cloud". For "brand the links for this domain", choose "No". Click on the
"Next" button.

For "Domain You Send From", enter "4949NN.xyz". Click on the
"Next" button.

Create the "CNAME" records identified. Click on the "Verify" button
once you are sure the "CNAME" records are visible.

#### Create a SendGrid API Key for SMTP Relay

Click on "Email API | Integration Guide". Choose "SMTP Relay". Name
your new API key. Click on the "Create Key" button. Make a note of
the server, ports, username, and password.

#### Frontend Changes

In `frontend/src/emailform.js`, add the code to 
import the Feathers client:

{% highlight javascript %}
import client from './feathers';
{% endhighlight %}

In the constructor, initialize the component's state
to empty:

{% highlight javascript %}
  constructor(props) {
    super(props);

    this.state = {};
  }
{% endhighlight %}

Add the code so that when the component mounts, we
update the state with a handle to the backend
"email" service:

{% highlight javascript %}
  componentDidMount() {
    const email = client.service('email');

    this.setState({email})
  }
{% endhighlight %}

Finally, we'll set up the event handler so that when the user fills
out the form and clicks "Send", we extract the "To:" addresss and
invoke the "create" method on the backend email service:

{% highlight javascript %}
  sendEmail(ev) {
    const inputTo = ev.target.querySelector('[id="to"]');
    const to = inputTo.value.trim();

    console.log( "To: " + to );

    this.state.email.create({
      to
    })
    .then(() => {
      inputTo.value = '';
    });

    ev.preventDefault();
  }
{% endhighlight %}


#### Backend Changes 

In the terminal where the backend is runnig, stop it (Ctrl-C).

Add a new hook to the application:

{% highlight shell %}
feathers generate hook
{% endhighlight %}

Answer the prompts as follows:

```
What is the name of the hook? sendEmail
What kind of hook should it be? after
What service(s) should this hook be for (select none to add it yourself)? email
What methods should the hook be for (select none to add it yourself)? create
```

In `backend/src/hooks/send-email.ts` add the code to extract
the result from the context and log it to the console:

{% highlight typescript %}
  return async (context: HookContext) => {
    const { result } = context;
    
    console.log( result );
    
    return context;
  };
{% endhighlight %}

Restart the backend (e.g. `npm run dev`).

Verify the application works end-to-end so that when you fill in
an email address in the frontend and "Send", you see the
email address logged on the backend.

We don't want our application to become a SPAM relay, so
create a list of safe recipients and check before we
actually send the email:

{% highlight typescript %}
  return async (context: HookContext) => {
    const { result } = context;
    
    // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["brian@koehler.ca", "bkoehler@langra.ca"];
    
    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }

    console.log( "GOOD: " + result['to']);
    
    return context;
  };
{% endhighlight %}

Stop the backend (Ctrl-C).

Instal nodemailer:

{% highlight shell %}
npm install --save nodemailer
npm install --save @types/nodemailer
{% endhighlight %}

Import the module into `backend/src/hooks/send-email.ts`:

{% highlight typescript %}
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';
{% endhighlight %}

Add the code to set up the transporter:

{% highlight typescript %}
  return async (context: HookContext) => {
    const { result } = context;
    
    // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["brian@koehler.ca", "bkoehler@langra.ca"];
    
    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }

    console.log( "GOOD: " + result['to']);
    
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey", // SendGrid user
        pass: "YOUR SENDGRID API PASSWORD GOES HERE!" // SendGrid password
      }
    });
    
    return context;
  };
{% endhighlight %}

Finally, add the code to send the actual eamil:

{% highlight typescript %}
    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"No Reply" <noreply@4949NN.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: "Your id is: " + result.id // plain text body
    });
    
    console.log("Message sent: %s", info.messageId);
{% endhighlight %}

Restart the backend (e.g. `npm run dev`) and test.
If you receive the email, you can click the
"Verify Integration" button in the SendGrid console.

## Assignment

1. Add a new in memory service 'sms' to the backend.
1. Add a new page 'SMS' to the frontend with a field for a phone number.
1. Create a Twilio account.
1. Integrate the frontend and backend to actaully send a SMS message.
