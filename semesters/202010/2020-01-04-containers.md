---
layout: page
title:  "Containers"
date:   2020-01-04 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- distinguish between a container and a virtual machine
- create a container
- mount a volume 
- map a port
- list and remove containers
- create an image
- remove an image


## Resources

- [Containerization Explained](https://youtu.be/0qotVMX-J5s){:target="_blank"} (video)
- [What is a Container?](https://youtu.be/EnJ7qX9fkcU){:target="_blank"} (video)
- [A Beginner-Friendly Introduction to Containers, VMs, and Docker](https://www.freecodecamp.org/news/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b/){:target="_blank"}
- [slides](slides/containers.html){:target="_blank"}

## Lab

### Creating a Container

Start AWS Cloud9 or Google Cloud Shell.

In a terminal, run the following command:

{% highlight shell %}
docker run --rm -it alpine
{% endhighlight %}

`docker` is the Docker client program.

`run` is the subcommand used create a new container.

`--rm` is the option to remove the container when it exits.

`-i` or `--interactive` keeps STDIN open even if not attached.

`-t` or `--tty` allocates a pseudo-TTY. `-i` and `-t` are used together to create a
container that we can interact with directly from the terminal.

`alpine` is the name of image upon which the new container is based.

If everything works, you should see a `# ` shell prompt and should be able
to use most basic shell commands such as `cd`, `ls`, `cat`, `cp`, `touch`, 
`echo`, `rm`, `mv` and `rmdir`.

Try the following:

{% highlight shell %}
cd /root
echo "Hello!" > first-file.txt
ls -la
cat first-file.txt
{% endhighlight %}

### Getting Information About Running Containers

Create a new terminal on the host.

Start another container:

{% highlight shell %}
docker run --rm -it alpine
{% endhighlight %}

Is `first-file.txt` present in the `/root` directory in this container?

Create yet another terminal on the host. Run the following command (ON THE HOST -- not on the containers)
to list the running containers:

{% highlight shell %}
docker ps -s
{% endhighlight %}

Run the following command to get streaming information on the running containers:

{% highlight shell %}
docker stats
{% endhighlight %}

You will have to hit `Ctrl-C` to stop `docker stats`.

Stop one of the contianers by entering `Ctrl-D` or `exit` on its terminal.

Confirm that the container has stopped by using `docker ps` or `docker stats`.

Stop the other container.

Where is `first-file.txt` now?

### Working with Images

You can get a list of locally available images using the following command:

{% highlight shell %}
docker images
{% endhighlight %}

If you are running out of disk space on your host, you can remove images that
you are no longer using as follows:

{% highlight shell %}
docker rmi cc0abc535e36
{% endhighlight %}

Here, `cc0abc535e36` is the image ID for the particular image we want to remove.

### Mounting Volumes

Frequently we will have a directory of preexisting files that we would like to
access inside the container or we end up creating files inside the container that
we want to persist beyond the lifetime of the container. In this case, we can use
the `-v` option to `docker run` to make a directory on the host available inside
the container. The value of the `-v` option has the following form: `/path/on/host:/path/inside/container`.
For example, run following commands on the host:

{% highlight shell %}
mkdir site1
cd site1
echo '<h1>First Page!</h1>' > index.html
docker run --rm -it -v "$PWD:/mnt/site1" alpine
{% endhighlight %}

Inside the container do the following:

{% highlight shell %}
cd /mnt/site1
cat index.html
echo '<h1>Second Page</h1>' > page2.html
{% endhighlight %}

and then exit the container. What is contained in the current directory on the host now?

### Mapping Ports

As with volumes, we often want to make network services running inside the container
accessible outside the container. In this case, we use the `-p` option to `docker run` to 
map a port on the host to a port inside the container. For example:

{% highlight shell %}
docker run --rm -v $PWD:/usr/share/nginx/html:ro -p 8080:80 -d nginx:alpine
{% endhighlight %}

Notice the use of the `-d` flag instead of `-it`. `-d` stands for daemon. This
container runs in the background and we do not interact with it directly with
the terminal. So in order to stop this container you would need to use the 
`docker stop xxx` command where `xxx` is either the name or ID of the container
which you could get from the `docker ps` command.

### Creating Images

Up until this point, we've used existing images from the Docker hub registry. There
are situations in which we would like to define our own images. For example, one of
the issues with the `alpine` image we've been using is that the users and groups defined
for the container do not mesh well with the users and groups in the Cloud9 VM (or the
Google Cloud Shell VM). This means that if new files are created on a mounted volume inside
the container, they may not have the right ownership or permissions to be easily accessed
outside the container. To correct this, we are going to create a new image.

Create a new directory, `Cloud9Alpine`.

Inside the directory, create a `Dockerfile` with the following contents:

{% highlight docker %}
FROM alpine:3.11.2

RUN addgroup -g 501 ec2-user \
    && adduser -u 501 -G ec2-user -s /bin/sh -D ec2-user \
    && apk add git
    
USER ec2-user
{% endhighlight %}

The `FROM` clause identifies the base image.

The `RUN` clause runs a command to update the image. In this case, 
adding the ec2-user group, the ec2-user user and installs git.

The `USER` clause identifies the defualt user to run commands as
inside the container. In this case, `ec2-user` so that files created
inside the container will have ownership and permissions so that
they can be accessed correctly from the host.

To build the image, run the following command from inside the `Cloud9Alpine`
directory:

{% highlight shell %}
docker build -t cloud9alpine .
{% endhighlight %}

The `-t` option identifies the tag for the image. The '.' argument identifies
the directory containing the `Dockerfile` to build from. In this case,
the current directory.

Start up the container as follows:

{% highlight shell %}
docker run --rm -it -v $PWD:/mnt/stuff cloud9alpine /bin/sh
{% endhighlight %}

Inside the container, execute the following commands to ensure that everything is working correctly:

{% highlight shell %}
git --version
cd /mnt/stuff
touch test.txt
{% endhighlight %}

Confirm that you are able to edit and save `test.txt` in the Cloud9 IDE. Exit the containter

## Assignment

1. Create a directory `ExpressJSContainerTest`.
1. Inside the directory, Create a `Dockerfile` based on the `node:13.5.0-alpine3.11` image.
1. The `Dockerfile` should add the ec2-user group and user and install the ExpressJS
    generator.
1. Build the image `expresstest`.
1. Create a container based on the new image with current directory
    (the directory with the `Dockerfile`) mounted inside and port 8080
    on the host forwarded to port 3000 in the container.
1. In the container, navigate to the mounted volume.
1. Use the ExpressGenerator to generate the standard ExpressJS skeleton app
    Make sure that the app gets generated in the same directory as the
    `Dockerfile`. For example, `express --veiw=pug .`
1. Add a new route for `/page2`.
1. Create a view for `/page2`.
1. Add a link from the index page to page2.
1. Run npm install.
1. Run npm start.
1. Verify that the app works.
1. Exit the container.
1. Create a project in GitLab and push the the assignment. Be sure to create a `.gitignore`
    file to exclude the `node_modules\` directory.

### Testing

The marker will test your submission as follows:

{% highlight shell %}
git clone your-repository
cd your-repository
docker build -t expressjs .
docker run --rm -it -v $PWD:/mnt/stuff -p 8080:3000 expressjs /bin/sh
{% endhighlight %}

And then inside the container:

{% highlight shell %}
cd /mnt/stuff
npm install
npm start
{% endhighlight %}

An then test the application in the browser.
