---
layout: page
title:  "Static Sites"
date:   2020-01-19 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- deploy SPA frontend to a static web host

## Resources

- [slides](slides/static-sites.html){:target="_blank"}
- [Firebase Hosting](https://firebase.google.com/docs/hosting){:target="_blank"}
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/){:target="_blank"}
- [GitHub Pages](https://help.github.com/en/github/working-with-github-pages/getting-started-with-github-pages){:target="_blank"}
- [AWS S3 + CloudFront](https://aws.amazon.com/blogs/networking-and-content-delivery/amazon-s3-amazon-cloudfront-a-match-made-in-the-cloud/){:target="_blank"}

## Lab

None, but consider serving the frontend of your project from
a static web host.

Here is a sample application where the backend is deployed to
an EC2 instance and the frontend is depolyed to Firebase Hosting:

`https://gitlab.com/langarabrian/firebase-ec2-demo`
