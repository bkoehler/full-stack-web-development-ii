---
layout: page
title:  "Authentication"
date:   2020-01-10 9:00:00 -0800
---

- TOC 
{:toc}

## Learning Outcomes

- implement local and OAuth authentication in an application

## Resources

- [OAuth 2.0 and OpenID Connect (in plain English)](https://www.youtube.com/watch?v=996OiexHze0){:target="_blank"} (video)
- [slides for above video](https://speakerdeck.com/nbarbettini/oauth-and-openid-connect-in-plain-english){:target="_blank"} 
- [slides](slides/authentication.html){:target="_blank"}

## Lab

### Setup

Start your Cloud9 environment. Go to the EC2 console. Copy the "Public DNS" for your Cloud9 instance.
Go to the Google Cloud Platform "Cloud DNS" tool and create 2 new CNAME records (`api1.4949NN.xyz` and
`app1.4949NN.xyz`) that point to your Cloud9 instance.

Sign into GitLab. Fork this project: `https://gitlab.com/langarabrian/auth1`. Remove the fork relationship.
Clone YOUR fork of the project in your Cloud9 environment.

Make top-level directories to hold the TLS certificates:

{% highlight shell %}
mkdir -p ~/etc/letsencrypt ~/var/lib/letsencrypt
{% endhighlight %}

Change to the project directory (e.g. `cd auth1`).

Start the MongoDB container: `scripts/start-mongodb-container.sh`

Start a new terminal. Change to the project directory.
Start an interactive container shell for the
backend: `scripts/start-backend-container.sh`

In the backend shell, install and run the Feathers backend:

{% highlight shell %}
npm install
npm run dev
{% endhighlight %}

Start a new terminal. Change to the project directory.
Start an interactive container shell for the
frontend: `scripts/start-frontend-container.sh`

In the backend shell, install and run the React frontend:

{% highlight shell %}
yarn install
yarn start
{% endhighlight %}

Edit the following files with your own domain names
(e.g. `api1.4949NN.xyz` and
`app1.4949NN.xyz`):

{% highlight shell %}
etc/nginx/conf.d/backend.conf
etc/nginx/conf.d/frontend.conf
frontend/src/feathers.js
{% endhighlight %}

Start a new terminal. Change to the project directory.
Start the Nginx 
container: `scripts/start-nginx-container.sh -it`

Test the backend by visiting the following URL: 

`http://api1.4949NN.xyz`

You should see the Feathers logo.

Test the frontend by visiting the following URL: 

`http://app1.4949NN.xyz`

You should see the shopping list form.

Add a few items to the shopping list. They don't show on the
frontend, but you can see them at the following backend URL:

`http://api1.4949NN.xyz/items`

### Adding Authentication

We are going to modify the application so that it requires
the user to authenticate before adding items to the list.

#### Initial Changes to Backend 

In the terminal where the backend is runnig, stop it (Ctrl-C).

Add a new service to the application:

{% highlight shell %}
feathers generate service
{% endhighlight %}

Answer the prompts as follows:

```
What kind of service is it? Mongoose
What is the name of the service? moreitems
Which path should the service be registered on? /moreitems
Does the service require authentication? Yes
```

Update `backend/src/models/moreitems.model.ts` so that
it has "description" and "quantity" fields just like
the "items" model.

Examine `backend/src/services/moreitems/moreitems.hooks.ts`

Restart the backend (e.g. `npm run dev`).

#### Initial Changes to Frontend 

Convince yourself that the authentication hook is working
by replacing `items` with `moreitems` in the frontend. On 
or about line 35 in `frontend/src/shoppinglist.js` replace
`client.service('items')` with `client.service('moreitems')`. 
Refresh the frontend page. Open the web console. Try to
add a new item. Note the error in the web console.

Create `frontend/src/loading.js` as follows:

{% highlight javascript %}
import React, { Component } from 'react';

class Loading extends Component {
  render() {
    return(<h1>Loading...</h1>);
  }
}

export default Loading;
{% endhighlight %}

Create `frontend/src/login.js` as follows:

{% highlight javascript %}
import React, { Component } from 'react';

class Login extends Component {
  render() {
    return(<h1>(login page goes here)</h1>);
  }
}

export default Login;
{% endhighlight %}

Update `frontend/src/application.js` as follows:

{% highlight javascript %}
import React, { Component } from 'react';
import Shoppinglist from './shoppinglist';
import Loading from './loading';
import Login from './login';
import client from './feathers';

class Application extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  
  render() {
    if ( this.state.login === undefined) {
      return(<Loading />);
    } else if ( this.state.login ) {
      return(<Shoppinglist />);
    }
    return(<Login />);
  }
}

export default Application;
{% endhighlight %}

Now if you refresh the application page, you should see the
"Loading" message. The next step is to test to see if
the user is already logged in during the "Loading" phase.
Add this method to the `Application` class:

{% highlight javascript %}
  componentDidMount() {
    // Try to authenticate with the JWT stored in localStorage
    client.authenticate().catch(() => this.setState({ login: null }));
  }
{% endhighlight %}

Now if you refresh the application page, you should see
the placeholder for the login page. The next step is to
create a button to login with Google. Update the `render`
method of the `Login` class as follows (be sure to
update "NN" with your own digits):

{% highlight javascript %}
  render() {
    return(
      <a className="btn btn-primary" 
         href="http://api1.4949NN.xyz/oauth/google" 
         role="button">Login with Google</a>);
  }
{% endhighlight %}

Now is you refresh the application page, you should
see a "Login with Google" button. If you click it,
you will get an error page. The next step is to
register our application with Google.

#### Final Changes to Backend 

Go to the [GCP Credentials 
Dashboard](https://console.cloud.google.com/apis/credentials){:target="_blank"}.

Click on "Create Credentials | OAuth client ID". If you haven't 
done so already, you will have to create an "OAuth Consent screen".
Fill out as follows:

```
Application name: CPSC 2650 Test
Support email: your email
Authorised domains: 4949NN.xyz
Application home page: http://app1.4949NN.xyz
```

Save the changes to the consent screen. Back on the OAuth client ID page,
choose "Web application" for application type. For name, choose 
whatever you want. For authorised redirect URIs, add:

```
http://api1.4949NN.xyz/oauth/google/callback
```

Finally click on the "Create" button. Enter the "Client ID" and
"Client secret" in `backend/config/default.json` for google 
key and secret respectively.

You will have to make some other changes to this file because
our front and back ends are running on different domains. The value
for oauth.redirect needs to be `http://app1.4949NN.xyz`. In the "google"
section you also need to set (with your own digits for "NN"):

```
"redirect_uri": "http://api1.4949NN.xyz/oauth/google/callback",
"host": "api1.4949NN.xyz",
"callback": "/oauth/google/authenticate",
```

Lastly, we need to make sure the email associated with the Google 
account is collected when the user logs in. Basically, you
can update `backend\authentication.ts` exactly as [for
GitLab in the 
tutorial](https://docs.feathersjs.com/guides/basics/authentication.html#github-login-oauth){:target="_blank"}.
Just rename "GitHubStrategy" with "GoogleStrategy".

#### Final Changes to Frontend

If you restart the backend and refresh the application page,
you should be able to log in with your Google account, but will
end up back on the application page, but stuck in the 
"Loading..." state. The last thing we need to do is detect
that the user has authenticated and update the application state.

Add the following in the `componentDidMount` method of the 
`Application` class right after the call to `client.authenticate()`:

{% highlight javascript %}
    // On successfull login ...
    client.on('authenticated', login => {
      
      // ... update the state
      this.setState({ login });
    });
{% endhighlight %}

At this point, if you refresh the application page, you should be taken
to the shopping list and able to add items.

## Assignment

1. Implement TLS for this exercise.
