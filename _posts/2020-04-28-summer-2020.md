---
layout: post
title:  "Summer 2020"
date:   2020-04-28 9:00:00 -0800
---

The following schedule is tentative and subject to change.

|Date|Topics|
|----|------|
|May 9|[Introduction to Containers](/semesters/202020/2020-05-09-containers.html)<br>[Review of Git and CI/CD](/semesters/202020/2020-05-09-ci-cd.html)|
|May 16|[Static Sites](/semesters/202020/2020-05-16-static-sites.html)<br>[Domain Name System (DNS)](/semesters/202020/2020-05-16-dns.html)|
|May 23|[REpresentational State Transfer (REST)](/semesters/202020/2020-05-23-rest.html)<br>[Transport Layer Security (TLS)](/semesters/202020/2020-05-23-tls.html)|
|May 30|[Complete Automated Public Turing test to tell Computers and Humans Apart (CAPTCHA)](/semesters/202020/2020-05-30-captcha.html)<br>[Authorization / Authentication](/semesters/202020/2020-05-30-authentication.html)|
|Jun 6|[Transactional/Notification Email](/semesters/202020/2020-06-06-email.html)<br>[Analytics](/semesters/202020/2020-06-06-analytics.html)|
|Jun 13|<strong>Midterm Exam</strong>|
|Jun 20|[CSS Frameworks](/semesters/202020/2020-06-20-css-frameworks.html)<br>[Serverless](/semesters/202020/2020-06-20-serverless.html)|
|Jun 27|Testing|
|Jul 4|[Real-time communication via WebSocket](/semesters/202020/2020-07-04-websocket.html)|
|Jul 11|[Parsing](/semesters/202020/2020-07-11-parsing.html)|
|Jul 18|Concurrency<br>[Internationalization (i18n)](/semesters/202020/2020-07-18-i18n.html)|
|Jul 25|[Accessibility (a11y)](/semesters/202020/2020-07-25-a11y.html)<br>[Single Page Applications with Angular](/semesters/202020/2020-07-25-angular.html)|
|Aug 1|Project Presentations<br>|
|Aug 8|<strong>Final Exam</strong>|
