---
layout: post
title:  "Spring 2020"
date:   2020-01-04 9:00:00 -0800
---

- [Containers](/semesters/202010/2020-01-04-containers.html)
- [CI/CD](/semesters/202010/2020-01-05-ci-cd.html)
- [DNS](/semesters/202010/2020-01-06-dns.html)
- [TLS](/semesters/202010/2020-01-07-tls.html)
- [REST](/semesters/202010/2020-01-08-rest.html)
- [CAPTCHA](/semesters/202010/2020-01-09-captcha.html)
- [Authentication](/semesters/202010/2020-01-10-authentication.html)
- [Parsing](/semesters/202010/2020-01-11-parsing.html)
- [Email](/semesters/202010/2020-01-12-email.html)
- [Analytics](/semesters/202010/2020-01-13-analytics.html)
- [Serverless](/semesters/202010/2020-01-14-serverless.html)
- [CSS Frameworks](/semesters/202010/2020-01-15-css-frameworks.html)
- [I18N](/semesters/202010/2020-01-16-i18n.html)
- [Websocket](/semesters/202010/2020-01-17-websocket.html)
- [A11Y](/semesters/202010/2020-01-18-a11y.html)
- [Angular](/semesters/202010/2020-01-19-angular.html)
- [Static Sites](/semesters/202010/2020-01-20-static-sites.html)
