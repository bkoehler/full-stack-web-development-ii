---
layout: post
title:  "Google Cloud Functions and TypeScript"
date:   2020-07-21 18:37:00 -0700
---

A student asked about deploying a [Feathers](https://feathersjs.com/)
application written in [TypeScript](https://www.typescriptlang.org/) to
[Google Cloud Functions](https://cloud.google.com/functions).
I purposely wrote the 
[Serverless lab](/semesters/202020/2020-06-20-serverless.html) 
using JavaScript to avoid this problem. The problem of course is
that Google Cloud Functions currently only supports
four runtimes: Node.js, Python, Go, and Java. It does not
support TypeScript directly.

Here is an outline of the solution for a Feathers application
written in TypeScript.

1. Re-define the `Application` type in `src/declarations.d.ts`
so that it depends on `FeathersApplication` from `@feathersjs/feathers`
instead of `ExpressFeathers` from `@feathersjs/express`.
1. Re-write `src/index.ts` so that it just has enough code
to instantiate the Feathers application (not the Express
application) and defines the Google Cloud Function entry point.
1. You can't deploy the TypeScript application directly, so you
will have to compile first to the `lib` directory and deploy from
there. In addition to the compiled TypeScript the `lib` directory
will need additional files to deploy to Google Cloud Functions:
    1. You will need to copy package\*.json to the `lib` directory.
    Unfortuanately, the definition of "main" in the package.json
    will cause the deploy to fail because there is no "src"
    subdirectory. So that line has to be removed from the
    package.json file. The package*.json files are needed
    becuase the deploy operation will do an "npm install --production"
    so it needs to know what the dependencies are.
    1. You will also need to copy the `config` directory
    to the `lib` directory. The deploy operation will pick
    up all the files in the source folder so if you set
    the source folder to the `lib` folder, then it needs
    to have a copy of the configuration files.
    1. After completing the steps above, you should
    be able to do a `gcloud functions deploy ... --source=lib`
    command from the root directory of the project.

Here is a complete example modelled on the starter
from [Serverless lab](/semesters/202020/2020-06-20-serverless.html)
except using TypeScript:

[gcfts-demo2](https://gitlab.com/langarabrian/gcfts-demo2)


