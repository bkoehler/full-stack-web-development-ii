#!/bin/bash

docker run --rm -it -p 8080:4000 -v "$PWD:/srv/jekyll" jekyll/jekyll:4.0.0 scripts/install-serve.sh
